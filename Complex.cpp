#include <cmath>
#include <iostream>

#include "Complex.h"

using namespace std;

/**
 * @brief 
 * @param actualPart 
 * @param imaginaryPart 
*/
Complex::Complex(double actualPart, double imaginaryPart)
{
    m_actual = actualPart;
    m_imaginary = imaginaryPart;
}

/**
 * @brief 
 * @param temporaryComplex 
*/
Complex::Complex(const Complex &temporaryComplex)
{
    m_actual = temporaryComplex.m_actual;
    m_imaginary = temporaryComplex.m_imaginary;
}

/**
 * @brief 
*/
Complex::~Complex()
{
    m_actual = 0.0;
    m_imaginary = 0.0;
}

/**
 * @brief 
 * @param actualPart 
 * @param imaginaryPart 
*/
void Complex::set(double actualPart, double imaginaryPart)
{
    m_actual = actualPart;
    m_imaginary = imaginaryPart;
}

/**
 * @brief 
*/
Complex::operator double()
{
    return abs();
}

/**
 * @brief 
 * @return 
*/
double Complex::abs()
{
    return sqrt(m_actual * m_actual + m_imaginary * m_imaginary);
}

/**
 * @brief 
 * @param temporaryComplex 
 * @return 
*/
Complex Complex::operator +(const Complex &temporaryComplex)
{
    Complex Result;

    Result.m_actual = m_actual + temporaryComplex.m_actual;
    Result.m_imaginary = m_imaginary + temporaryComplex.m_imaginary;

    return Result;
}

/**
 * @brief 
 * @param temporaryComplex 
 * @return 
*/
Complex Complex::operator -(const Complex &temporaryComplex)
{
    Complex Result;

    Result.m_actual = m_actual - temporaryComplex.m_actual;
    Result.m_imaginary = m_imaginary - temporaryComplex.m_imaginary;

    return Result;
}

/**
 * @brief 
 * @param actualPart 
 * @return 
*/
Complex Complex::operator +(const double &actualPart)
{
    Complex Result;

    Result.m_actual = m_actual + actualPart;
    Result.m_imaginary = m_imaginary;

    return Result;
}

/**
 * @brief 
 * @param actualPart 
 * @return 
*/
Complex Complex::operator -(const double &actualPart)
{
    Complex Result(*this);

    Result.m_actual = m_actual - actualPart;

    return Result;
}

/**
 * @brief 
 * @param temporaryComplex 
 * @return 
*/
Complex Complex::operator *(const Complex &temporaryComplex)
{
    Complex Result;

    Result.m_actual = m_actual * temporaryComplex.m_actual - m_imaginary * temporaryComplex.m_imaginary;
    Result.m_imaginary = m_actual * temporaryComplex.m_imaginary + m_imaginary * temporaryComplex.m_actual;

    return Result;
}

/**
 * @brief 
 * @param multiplicationValue 
 * @return 
*/
Complex Complex::operator *(const double &multiplicationValue)
{
    Complex Result;

    Result.m_actual = m_actual * multiplicationValue;
    Result.m_imaginary = m_imaginary * multiplicationValue;

    return Result;
}

/**
 * @brief 
 * @param divisionValue 
 * @return 
*/
Complex Complex::operator /(const double &divisionValue)
{
    Complex Result;

    Result.m_actual = m_actual / divisionValue;
    Result.m_imaginary = m_imaginary / divisionValue;

    return Result;
}

/**
 * @brief 
 * @param summaryValue 
 * @return 
*/
Complex& Complex::operator +=(const Complex &summaryValue)
{
    m_actual += summaryValue.m_actual;
    m_imaginary += summaryValue.m_imaginary;

    return *this;
}

/**
 * @brief 
 * @param differenceValue 
 * @return 
*/
Complex& Complex::operator -=(const Complex &differenceValue)
{
    m_actual -= differenceValue.m_actual;
    m_imaginary -= differenceValue.m_imaginary;

    return *this;
}

/**
 * @brief 
 * @param multiplicationValue 
 * @return 
*/
Complex& Complex::operator *=(const Complex &multiplicationValue)
{
    double temporaryImaginaryPart = m_actual;

    m_actual = m_actual * multiplicationValue.m_actual - m_imaginary * multiplicationValue.m_imaginary;
    m_imaginary = m_imaginary * multiplicationValue.m_actual + temporaryImaginaryPart * multiplicationValue.m_imaginary;

    return *this;
}

/**
 * @brief 
 * @param actualPart 
 * @return 
*/
Complex& Complex::operator +=(const double &actualPart)
{
    m_actual += actualPart;

    return *this;
}

/**
 * @brief 
 * @param differenceValue 
 * @return 
*/
Complex& Complex::operator -=(const double &differenceValue)
{
    m_actual -= differenceValue;

    return *this;
}

/**
 * @brief 
 * @param multiplicationValue 
 * @return 
*/
Complex& Complex::operator *=(const double &multiplicationValue)
{
    m_actual *= multiplicationValue;
    m_imaginary *= multiplicationValue;

    return *this;
}

/**
 * @brief 
 * @param divisionValue 
 * @return 
*/
Complex& Complex::operator /=(const double &divisionValue)
{
    m_actual /= divisionValue;
    m_imaginary /= divisionValue;

    return *this;
}

/**
 * @brief 
 * @param newComplex 
 * @return 
*/
Complex& Complex::operator =(const Complex &newComplex)
{
    m_actual = newComplex.m_actual;
    m_imaginary = newComplex.m_imaginary;

    return *this;
}

/**
 * @brief 
 * @param newActualPart 
 * @return 
*/
Complex& Complex::operator =(const double &newActualPart)
{
    m_actual = newActualPart;
    m_imaginary = 0.0;

    return *this;
}

/**
 * @brief 
 * @param inputStream 
 * @param setComplex 
 * @return 
*/
istream& operator >>(istream &inputStream, Complex &setComplex)
{
    char temporaryBuffer[256];

    inputStream >> setComplex.m_actual >> setComplex.m_imaginary >> temporaryBuffer;

    return inputStream;
}

/**
 * @brief 
 * @param outputStream 
 * @param getComplex 
 * @return 
*/
ostream& operator <<(ostream &outputStream, Complex &getComplex)
{
    outputStream << getComplex.m_actual;

    if (!(getComplex.m_imaginary < 0)) {
        outputStream << '+';
    }

    outputStream << getComplex.m_imaginary << 'i';

    return outputStream;
}

/**
 * @brief 
 * @param actualPart 
 * @param temporaryComplex 
 * @return 
*/
Complex operator +(const double &actualPart, const Complex &temporaryComplex)
{
    Complex Result;

    Result.m_actual = actualPart + temporaryComplex.m_actual;
    Result.m_imaginary = temporaryComplex.m_imaginary;

    return Result;
}

/**
 * @brief 
 * @param actualPart 
 * @param temporaryComplex 
 * @return 
*/
Complex operator -(const double &actualPart, const Complex &temporaryComplex)
{
    Complex Result;

    Result.m_actual = actualPart - temporaryComplex.m_actual;
    Result.m_imaginary = -temporaryComplex.m_imaginary;

    return Result;
}

/**
 * @brief 
 * @param multiplicationValue 
 * @param multiplicationComplex 
 * @return 
*/
Complex operator *(const double &multiplicationValue, const Complex &multiplicationComplex)
{
    Complex R;

    R.m_actual = multiplicationValue * multiplicationComplex.m_actual;
    R.m_imaginary = multiplicationValue * multiplicationComplex.m_imaginary;

    return R;
}
